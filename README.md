# README #

eagle-pipeline-qc 

Version: 0.1

This Python project is for asserting that a given ehive pipeline dataset is a valid, corruption-free file conforming to the individual pipeline's specifications. 

### What is this repository for? ###

* Provides various file integrity and file content tests

### How do I get set up? ###

* Download from Bitbucket: git@bitbucket.org:eaglegenomics/eagle-pipeline-qc.git
* Change the main function according to the pipeline of interest. Choose tests suitable.
* Dependencies: zip and gzip python libraries (provided with standard Python installations
* Deployment instructions: 

To install as a package: 
python setup.py install

### How to run it? ###

python QC/input_check.py test.zip /mnt

- The first argument, test.zip, is the input file to check.

- The second argument, "/mnt" is a folder where the program can extract the contents of the zip file, it is optional, if not given, it uses your operating system's default temp directory. 

### Contribution guidelines ###

* Add your new unit test function into QC/input_test.py
* Submit a pull request

### Who do I talk to? ###

* Repo owner: Mutlu Dogruel
* Contact: mutlu.dogruel@eaglegenomics.com