__author__ = 'mutlu'

import sys
import os
import unittest
import zipfile
import gzip
import tempfile
from sys import platform as _platform

from util.process import runCommand


class ZipFileClassTests(unittest.TestCase):
    def __init__(self, testname, path, tempdir=None):
        super(ZipFileClassTests, self).__init__(testname)
        self.testFile = path
        self.tempDir = tempdir

    def test_input_exists(self):
        """
        A simple test to check if the input file exists
        """
        self.assertTrue( os.path.isfile(self.testFile), msg="Could not find input file, check your path and re-run!" )

    def test_input_via_file_command(self):
        """
        Checks input zip file's integrity by using the zip library
        Some zip files slightly modified by text editors observed to pass this test, as long as the zip header is intact
        """
        self.assertTrue( zipfile.is_zipfile(self.testFile), msg="Not a valid zip file!" )
        for status, line in runCommand("file " + self.testFile):
            self.assertTrue(status)
            if len(line) > 0:
                self.assertTrue("Zip archive data" in line, msg="Possibly corrupt zip file!")

    def test_input_via_nativeZipTesting(self):
        """
        Zip integrity test using the command line tool zipinfo
        Provides a robust way of asserting file integrity
        It's available for Linux and Mac systems
        This test will fail on Windows, so please exclude this function if running the test suite from Windows
        """
        cmd = ""
        if _platform == "linux" or _platform == "linux2":
            cmd = "zip -t "
        elif _platform == "darwin":
            cmd = "zip -T "
        elif _platform == "win32":
            self.assertTrue(False, msg="Your Windows platform is not supported by this test!")
        for status, line in runCommand(cmd + self.testFile):
            self.assertTrue(status, "zipinfo reported problems while checking file integrity!")

    def test_dir_structure(self):
        """
        This function asserts that the input zip file consists of top-level folders
        for each sample. Each folder will typically have one forward and one reverse read file
        which has to be compressed by gzip having a ".gz" extension. The main container file
        is a regular zip file!
        """
        zfile = zipfile.ZipFile(self.testFile)
        for info in zfile.infolist():
            if not info.filename.endswith('/'):
                self.assertTrue(info.filename.count('/') == 1, msg='Wrong directory structure in zip file!')
                self.assertTrue(info.filename.endswith('.gz'), msg='Samples are not gzip compressed!')

    def test_input_readfile_sizes(self):
        """
        Checks the zip file's components for their sizes.
        Eliminates samples with zero bytes.
        Currently minimum sample size threshold is set to 500 bytes
        :return:
        """
        zfile = zipfile.ZipFile(self.testFile)
        for info in zfile.infolist():
            if not info.filename.endswith('/'):
                self.assertTrue(info.file_size > 500, msg=info.filename + " in zip file is too small!")
                #print '\tUncompressed:\t', info.filename, info.file_size, 'bytes'

    def test_gc_content(self):
        """
        This functions tests:
        1) The individual reads are valid FASTQ formatted
        2) The reads' GC contents are at least 20% and at most 80%, (normally around 40-60%)
        Low or high GC content is a sign of invalid data, contaminated DNA, DNA that will spoil analysis results
        The GC content is checked for each sample, not for every sequence in the sample file
        """
        zfile = zipfile.ZipFile(self.testFile)
        assertions = []
        for info in zfile.infolist():
            if not info.filename.endswith('/'):
                try:
                    data = zfile.read(info.filename)
                    if self.tempDir == None:
                        self.tempDir = tempfile.gettempdir()
                    gz_file = self.tempDir + "/" + info.filename.split("/")[1]
                    print "extracting into " + gz_file
                    f_out = open(gz_file, 'wb')
                    f_out.writelines(data)
                    f_out.close()
                    a = 0
                    t = 0
                    g = 0
                    c = 0
                    header  = False
                    seq     = False
                    gh = gzip.open(gz_file, 'rb')
                    file_content = gh.readlines()
                    lineCount = 0
                    for line in file_content:
                        lineCount += 1
                        line = line.strip()
                        if line == '' or line == '+':
                            continue
                        if not header:
                            header = True
                            if not line.startswith('@'):
                                msg = "AssertionError: Fastq format issue with " +  info.filename.split("/")[1] + " around line " + str(lineCount)
                                assertions.append(msg)
                                break
                            continue
                        if not seq:
                            g += line.count("G")
                            c += line.count("C")
                            a += line.count("A")
                            t += line.count("T")
                            seq = True
                            continue
                        header = False
                        seq    = False
                    gh.close()
                    totalBaseCount = a + t + c + g
                    gcCount = g + c
                    gcFraction = 100 * float(gcCount) / (0.1 + totalBaseCount)
                    if gcFraction < 25:
                        msg = "AssertionError: GC content is too low -- " + str(gcFraction) + "% in " + info.filename.split("/")[1]
                        assertions.append(msg)
                    elif gcFraction > 75:
                        msg = "AssertionError: GC content is too high -- " + str(gcFraction) + "% in " + info.filename.split("/")[1]
                        assertions.append(msg)
                except:
                    raise

        # List all assertion issues found by this test
        for msg in assertions:
            print msg

        # Throw a single assertion error for all problems found by this test
        if len( assertions ) > 0:
            self.assertTrue(False, msg="Problems in input file " + self.testFile )


if __name__ == "__main__":
    path = sys.argv[1]
    tempdir = None
    if len(sys.argv) == 3:
        tempdir = sys.argv[2]
    suite = unittest.TestSuite()
    suite.addTest(ZipFileClassTests("test_input_exists", path))
    suite.addTest(ZipFileClassTests("test_input_via_file_command", path))
    suite.addTest(ZipFileClassTests("test_input_via_nativeZipTesting", path))
    suite.addTest(ZipFileClassTests("test_dir_structure", path))
    suite.addTest(ZipFileClassTests("test_input_readfile_sizes", path))
    suite.addTest(ZipFileClassTests("test_gc_content", path, tempdir))

    unittest.TextTestRunner().run(suite)