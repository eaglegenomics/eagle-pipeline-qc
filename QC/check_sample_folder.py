__author__ = 'mutlu'

import sys
import os
import gzip

class SampleFolderTests():
    def __init__(self, path):
        if path:
            if not path.endswith("/"):
                path += '/'
        self.testFolder = path
        self.samples    = []
        self.messages   = []
        self.R1_file    = None
        self.R2_file    = None

    def test_folder_exists(self):
        """
        A simple test to check if the folder exists
        """
        print self.testFolder
        if not os.path.isdir(self.testFolder):
            self.messages.append("QC FAIL: Could not find the input folder " + self.testFolder)

    def test_scanFolderForR1andR2(self):
        """
        Check we have R1 and R2 read files
        :return: True or False
        """
        for folder in os.listdir(self.testFolder):
            R1 = False
            R2 = False
            R1_file = None
            R2_file = None
            folderPath = os.path.join(self.testFolder,folder)
            if os.path.isdir(folderPath):
                self.samples.append(folder)
                for fname in os.listdir(folderPath):
                    samplePath = os.path.join(folderPath, fname)
                    if "_R1_" in fname or "_forward_" in fname:
                        R1 = True
                        R1_file = samplePath
                    elif "_R2_" in fname or "_reverse_" in fname:
                        R2 = True
                        R2_file = samplePath
            if not R1:
                self.messages.append("QC FAIL: Could not find forward reads file for sample " + folder)
            elif not R1_file.endswith(".gz"):
                self.messages.append("QC FAIL: Forward reads file for sample " + folder + " does not end with .gz")
            elif self.test_gc_content(R1_file):
            if not R2:
                self.messages.append("QC FAIL: Could not find reverse reads file for sample " + folder)
            elif not R2_file.endswith(".gz"):
                self.messages.append("QC FAIL: Reverse reads file for sample " + folder + " does not end with .gz")
                self.test_gc_content(R2_file)

    def test_gc_content(self, gz_file):
        """
        This functions tests:
        1) The individual reads are valid FASTQ formatted
        2) The reads' GC contents are at least 25% and at most 75%, (normally around 40-60%)
        Low or high GC content is a sign of invalid data, contaminated DNA, DNA that will spoil analysis results
        The GC content is checked for each sample, not for every sequence in the sample file
        """
        if not gz_file:
            self.messages.append("QC FAIL: no gz file for sample " + str(self.sampleName) )
            return
        try:
            a = 0
            t = 0
            g = 0
            c = 0
            header  = False
            seq     = False
            gh = gzip.open(gz_file, 'rb')
            file_content = gh.readlines()
            lineCount = 0
            for line in file_content:
                lineCount += 1
                line = line.strip()
                if line == '' or line == '+':
                    continue
                if not header:
                    header = True
                    if not line.startswith('@'):
                        msg = "QC FAIL: Fastq format issue around line " + str(lineCount) + " in " + self.testFolder + "/" + os.path.basename(gz_file)
                        self.messages.append(msg)
                        break
                    continue
                if not seq:
                    g += line.count("G")
                    c += line.count("C")
                    a += line.count("A")
                    t += line.count("T")
                    seq = True
                    continue
                header = False
                seq    = False
            gh.close()
            totalBaseCount = a + t + c + g
            gcCount = g + c
            gcFraction = 100 * float(gcCount) / (0.1 + totalBaseCount)
            if gcFraction < 25:
                msg = "QC FAIL: GC content is too low -- " + str(gcFraction) + "% in " + os.path.basename(gz_file)
                self.messages.append(msg)
            elif gcFraction > 75:
                msg = "QC FAIL: GC content is too high -- " + str(gcFraction) + "% in " + os.path.basename(gz_file)
                self.messages.append(msg)
            else:
                return True
        except:
            print "ERROR: Unexpected QC failure for sample " + self.sampleName
            raise

    def writeMessages(self):
        for msg in self.messages:
            print msg

if __name__ == "__main__":
    path = sys.argv[1]
    foldertest = SampleFolderTests(path)
    foldertest.test_folder_exists()
    foldertest.test_scanFolderForR1andR2()
    foldertest.test_name_ends_with_gz()
    foldertest.test_gc_content(foldertest.R1_file)
    foldertest.test_gc_content(foldertest.R2_file)
    foldertest.writeMessages()
