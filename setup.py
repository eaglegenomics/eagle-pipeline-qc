from distutils.core import setup

setup(
    name='eagle-qc',
    version='0.1',
    packages=['QC', 'util'],
    url='git@bitbucket.org:eaglegenomics/eagle-pipeline-qc.git',
    license='Eagle Genomics',
    author='mutlu',
    author_email='mutlu.dogruel@eaglegenomics.com',
    description=''
)
