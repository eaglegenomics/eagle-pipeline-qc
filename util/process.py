__author__ = 'mutlu'
import subprocess
import select

def runCommand(cmd):
    #print cmd
    proc = subprocess.Popen( [cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True )
    while True:
        reads = [proc.stdout.fileno(), proc.stderr.fileno()]
        ret = select.select(reads, [], [])
        for fd in ret[0]:
            if fd == proc.stdout.fileno():
                line = proc.stdout.readline()
                if len(line) > 0:
                    yield True, line
            if fd == proc.stderr.fileno():
                line = proc.stderr.readline().lower()
                if "error" in line or "corrupt" in line:
                    yield False, line
        if proc.poll() != None:
            break
